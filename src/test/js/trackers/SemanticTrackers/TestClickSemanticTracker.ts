///<reference path="../../../../../typings/index.d.ts" />

import {SemanticTrackerTest} from "./TestSemanticTracker";
import {ClickSemanticTracker} from "../../../../main/js/trackers/SemanticTrackers/ClickSemanticTracker";

SemanticTrackerTest(ClickSemanticTracker);

describe("The Semantic Click Tracker", function () {
    
    beforeEach(function () {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        this.tracker = new ClickSemanticTracker();
        this.collector = jasmine.createSpyObj("collector", ["sendMessage"]);
        this.element = jasmine.createSpyObj("element", ["addEventListener"]);
        this.tracker.withCollector(this.collector);
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it("should register the element correctly.", function () {
        this.element.addEventListener.and.callFake((_: string, callback: any) => {
            callback();
        });

        this.tracker.registerElement(this.element, "Comment textfield");
        expect(this.collector.sendMessage).toHaveBeenCalledTimes(1);
    });
});