/// <reference path="./../../../../main/js/trackers/RawDataTrackers/Tracker.d.ts" />
///<reference path="../../../../../typings/index.d.ts" />

export function testTracker(TrackerClass: new () => Tracker) {
    describe("The basic tracking behaviour", function() {
        
        beforeEach(function() {
            this.fakeCollector = jasmine.createSpyObj("Collector", ["sendMessage"]);
            this.fakeThrottle = jasmine.createSpyObj("Throttle", ["sendMessage"]);
            this.fakeThrottleFac = () => this.fakeThrottle;
            this.fakeMessage = {
                test: "Works"
            };
            
            this.tracker = new TrackerClass();
            this.tracker.withCollector(this.fakeCollector).withThrottle(this.fakeThrottleFac);
            this.tracker.sendMessage(this.fakeMessage);
        });
        
        it("should allow adding an throttle in between.", function () {
            expect(this.fakeThrottle.sendMessage).toHaveBeenCalled();
            expect(this.fakeCollector.sendMessage).not.toHaveBeenCalled();
        });
    });
}