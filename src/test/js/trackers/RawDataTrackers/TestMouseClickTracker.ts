///<reference path="../../../../../typings/index.d.ts" />

import createSpyObj = jasmine.createSpyObj;
import {MouseClickTracker} from "../../../../main/js/trackers/RawDataTrackers/MouseClickTracker";
import {testTracker} from "./TestTracker";

describe("The Mouse Click Tracker", function() {
    testTracker(MouseClickTracker);

    beforeEach(function() {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        const _this = this;
        this.eventCall = <(event: any) => void> null;
        this.mousePositionObject = {
            pageX: 15,
            pageY: 25,
            clientX: 15,
            clientY: 25
        };
        this.anotherMousePositionObject = {
            pageX: 35,
            pageY: 45,
            clientX: 15,
            clientY: 25
        };
        
         // Capture any added eventlisteners.
        document.addEventListener = function (ev: string, func: (event: any) => void) {
            _this.eventCall = func;
        };
        
        
        this.tracker = new MouseClickTracker();
        spyOn(this.tracker, "sendClickData").and.callThrough();
        spyOn(this.tracker, "sendPositionData").and.callThrough();
        this.collector = createSpyObj("TrackingCollector", ["sendMessage"]);
        this.tracker.withCollector(this.collector);
        this.tracker.register();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    it("should not call sendData on non clicks", function() {
        expect(this.collector.sendMessage).not.toHaveBeenCalled();
    });

    it("should call sendData on clicks", function() {
        // Simulate a mouse click
        this.eventCall(this.mousePositionObject);
        
        expect(this.collector.sendMessage).toHaveBeenCalledWith(
            jasmine.objectContaining({
                data: {
                    created_at: Date.now() / 1000
                }
            })
        );
        
        expect(this.collector.sendMessage).toHaveBeenCalledWith(
            jasmine.objectContaining({
                data: {
                    position_x: this.mousePositionObject.pageX,
                    position_y: this.mousePositionObject.pageY,
                    viewport_x: this.mousePositionObject.clientX,
                    viewport_y: this.mousePositionObject.clientY,
                    created_at: Date.now() / 1000
                }
            })
        );
    });

    it("should call sendData more often after multiple clicks", function() {
        // Simulate mouse clicks.
        this.eventCall(this.mousePositionObject);
        this.eventCall(this.anotherMousePositionObject);
        
        expect(this.collector.sendMessage).toHaveBeenCalledTimes(4);
    });
});
